+++
date = "2017-03-09T13:23:28+08:00"
draft = false
title = "join"

+++

Joining our community couldn't be simpler! If you're not a member of our Slack, you can [request an invite](http://links.devtech.scot/slack).

⤹ We welcome code contributions, banter and blog articles. All the links you need are ⤸
