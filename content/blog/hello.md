+++
date = "2017-03-12T11:55:32+08:00"
title = "Hello, world!"

+++

As you can probably tell, things are a little unripe at the moment. We're still putting things together and would appreciate some help.

Contribute at [GitLab](https://gitlab.com/devtechscot/www.devtech.scot)
